﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using TurretDefence.Helpers;
using Turretdefence.Helpers;

namespace Turretdefence.Hubs
{
    public class GameHub : Hub
    {
        public override Task OnConnected()
        {
            return base.OnConnected();
        }

        public override Task OnDisconnected()
        {
            var room = Repository.DisconnectUser(Context.ConnectionId);
            Clients.Group(room.ID).updatePlayerList(room.Players);
            Clients.Group(room.ID).playerDisconnected(Context.ConnectionId);
            return base.OnDisconnected();
        }

        public void CreateRoom(string name, string password)
        {
            var gameRoom = Repository.CreateRoom(name, password);
            var player = Repository.CreatePlayer("Henrik", Context.ConnectionId, gameRoom);
            gameRoom.Players.Add(player);

            Groups.Add(Context.ConnectionId, gameRoom.ID);
            Clients.Caller.joinRoom(gameRoom);
        }
        
        public void JoinRoom(string roomID, string password)
        {
            var gameRoom = Repository.GetRoom(roomID);
            if (gameRoom.GameStarted == false)
            {
                var player = Repository.CreatePlayer("Penke", Context.ConnectionId, gameRoom);
                gameRoom.Players.Add(player);

                Groups.Add(Context.ConnectionId, gameRoom.ID);
                Clients.Caller.joinRoom(gameRoom);
                Clients.Caller.setPlayer(gameRoom.Players.IndexOf(player));
                Clients.Group(gameRoom.ID).updatePlayerList(gameRoom.Players);
            }
        }

        public void StartGame()
        { 
            var gameRoom = Repository.GetRoom(Repository.GetPlayer(Context.ConnectionId).RoomID);
            gameRoom.Tick = () => {
                Clients.Group(gameRoom.ID).startWave();
            };
            gameRoom.StartGame();
            Clients.Group(gameRoom.ID).gameStarted(gameRoom.Players);
        }

        public void AbortGame()
        {
            var gameRoom = Repository.GetRoom(Repository.GetPlayer(Context.ConnectionId).RoomID);
            gameRoom.EndGame();
            Clients.Group(gameRoom.ID).gameEnded();
        }

        public void GetRoomlist()
        {
            Clients.Caller.setRoomList(Repository.GetRoomList());
        }

        public void WalkTo(int player, int x, int y)
        {
            Clients.All.playerGoesTo(player, x, y);
        }

        public void StartConstruction(int player, int turretId, int x, int y)
        {
            Clients.All.playerStartsConstruction(player, turretId, x, y);
        }
    }
}