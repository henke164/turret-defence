﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TurretDefence.Helpers;

namespace Turretdefence.Helpers
{
    public class Repository
    {
        static List<GameRoom> GameRooms = new List<GameRoom>();

        public static Player GetPlayer(string connectionID)
        {
            var gameRoom = GameRooms.Where(g => g.Players.Select(i => i.ConnectionID).Contains(connectionID)).FirstOrDefault();
            return gameRoom.Players.Where(p => p.ConnectionID == connectionID).FirstOrDefault();
        }

        public static List<GameRoom> GetRoomList()
        {
            List<GameRoom> list = new List<GameRoom>();
            foreach (var gameRoom in GameRooms.Where(b=>b.GameStarted == false))
            {
                list.Add(new GameRoom()
                {
                    ID = gameRoom.ID,
                    Name = gameRoom.Name,
                    Players = gameRoom.Players
                });
            }
            
            return list;
        }

        public static GameRoom DisconnectUser(string connectionID)
        {
            var currentPlayer = GetPlayer(connectionID);
            var gameRoom = GameRooms.Where(id => id.ID == currentPlayer.RoomID).FirstOrDefault();
            gameRoom.Players.Remove(currentPlayer);
            return gameRoom;
        }

        public static GameRoom CreateRoom(string name, string password)
        {
            var gameRoom = new GameRoom();
            gameRoom.ID = Guid.NewGuid().ToString();
            gameRoom.Name = name;
            gameRoom.Password = password;
            GameRooms.Add(gameRoom);
            return gameRoom;
        }

        public static Player CreatePlayer(string name, string connectionID, GameRoom room)
        {
            var player = new Player()
            {
                Name = name,
                ConnectionID = connectionID,
                RoomID = room.ID
            };
            return player;
        }

        public static GameRoom GetRoom(string roomID)
        {
            var gameRoom = GameRooms.Where(s => s.ID == roomID).FirstOrDefault();
            return gameRoom;
        }
    }
}