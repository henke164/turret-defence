﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace TurretDefence.Helpers
{
    public class GameRoom
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }

        public bool GameStarted = false;

        public bool GameEnded = false;

        public List<Player> Players = new List<Player>();

        public Action Tick;

        DateTime lastTick;
        
        public void StartGame()
        {
            if (GameStarted == false)
            {
                GameStarted = true;
                CancellationTokenSource cancel = new CancellationTokenSource();
                CancellationToken ct = cancel.Token;

                var task = Task.Factory.StartNew(() =>
                {
                    ct.ThrowIfCancellationRequested();

                    while (true)
                    {
                        if (ct.IsCancellationRequested)
                            ct.ThrowIfCancellationRequested();

                        if (GameEnded)
                            cancel.Cancel();

                        if (lastTick == null || DateTime.Now > lastTick.AddSeconds(10))
                        { 
                            lastTick = DateTime.Now;
                            Tick();
                        }
                    }
                }, cancel.Token);
            }
        }

        public void EndGame()
        {
            GameEnded = true;
        }
    }
    public class Player
    {
        public string Name { get; set; }
        public string ConnectionID { get; set; }
        public string RoomID { get; set; }
    }
}