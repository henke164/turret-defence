﻿var Missile = function (startPosition) {
    GameObject.call(this);
    this.speed = 10;

    this.target = null;
    this.onHitCallback = null;

    this.update = function () {
        if (this.target != null && this.onHitCallback != null) {
            this.walkTo(this.target.position.x / Game.TileSize.width, this.target.position.y / Game.TileSize.width, function () {
                this.onHitCallback();
            });
        }
        Missile.prototype.update.call(this);
    };

    this.draw = function () {
        Missile.prototype.draw.call(this);
    };

    this.initialize("missile", Textures.missile(), startPosition, 0, false);
};

Missile.prototype = GameObject.prototype;