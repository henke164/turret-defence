﻿(function (win) {
    return win.Game = new function () {
        this.Canvas = {};
        this.Context = {};
        this.PreRenderCanvas = {};
        this.PreRenderContext = {};
        this.TileSize = { width: 50, height: 50 };

        this.updateGameSize = function () {
            this.Canvas.width = $("#wrapper").width();
            this.Canvas.height = ((this.Canvas.width / 4) * 3) - 400;
            this.PreRenderCanvas.width = this.Canvas.width;
            this.PreRenderCanvas.height = this.Canvas.height;

            this.TileSize = {
                width: this.Canvas.width / 20,
                height: this.Canvas.width / 20
            };
        };

        this.initializeGame = function (canvasElement) {
            this.Canvas = canvasElement;
            this.Context = this.Canvas.getContext("2d");
            this.PreRenderCanvas = document.createElement('canvas');
            this.PreRenderContext = this.PreRenderCanvas.getContext('2d');
            this.updateGameSize();
            
            Textures.loadTextures();
            Sound.loadSounds();
        };

        this.getTurretList = function () {
            var turrets = [{
                ID: 1,
                Image: 'Content/Images/menu_turret_1.jpg',
                Name: 'Water Cannon',
                Price: 15
            }, {
                ID: 2,
                Image: 'Content/Images/menu_turret_1.jpg',
                Name: 'Pew cannon',
                Price: 25
            }];
            return turrets;
        };

        this.draw = function () {
            drawgame();
        }

        function drawgame() {
            if (Game.Context != null)
                Game.Context.drawImage(Game.PreRenderCanvas, 0, 0);
        }
    }
})(window);


var GameCtrl = ['$scope', function ($scope) {
    $scope.currentPlayer = 0;

    var gameHub = $.connection.gameHub;
    var currentLevel = new Level();
    var hud = new Interface();
    document.onkeydown = hud.checkKey;

    var builders = [];
    $scope.loadingGame = false;
    $scope.allRooms = [];
    $scope.currentRoom = {};
    $scope.roomName = "";
    $scope.roomPassword = "";
    $scope.gameState = 'menu';
    $scope.allEnemies = [];
    $scope.myCannons = [];
    $scope.turretList = [];
    $scope.markerSet = false;

    $scope.browseForRoom = function () {
        gameHub.server.getRoomlist();
        $scope.gameState = 'browseRoom';
    };

    $scope.createRoom = function () {
        gameHub.server.createRoom($scope.roomName, $scope.roomPassword);
    };

    $scope.joinRoom = function (room) {
        gameHub.server.joinRoom(room.ID, "");
    };

    $scope.endGame = function () {
        gameHub.server.abortGame();
    };

    $scope.startGame = function () {
        if ($scope.loadingGame == false)
            gameHub.server.startGame();
    };

    var update = function () {
        // update functions
        $(builders).each(function (i, builder) {
            builder.update();
        });

        $($scope.allEnemies).each(function (i, enemy) {
            enemy.update();
        });

        // Draw objects 
        currentLevel.draw();
        $($scope.myCannons).each(function (i, cannon) {
            cannon.update();
            cannon.draw();
        });

        $(builders).each(function (i, builder) {
            builder.draw();
        });

        $($scope.allEnemies).each(function (i, enemy) {
            enemy.draw();
        });

        // Draw huds
        Game.Context.beginPath();
        $($scope.myCannons).each(function (i, cannon) {
            cannon.drawHud();
        });

        $($scope.allEnemies).each(function (i, enemy) {
            enemy.drawHud();
        });
        
        $(builders).each(function (i, builder) {
            builder.drawHud();
        });

        hud.draw();
        Game.draw();
        Game.Context.stroke();
        requestAnimationFrame(update);
    };
    
    window.onresize = function () {
        Game.updateGameSize();
    }

    $scope.initGame = function () {
        Game.initializeGame($("#gamescreen")[0]);
        $scope.turretList = Game.getTurretList();
        currentLevel.initialize('sand');
        hud.initialize();
    };
   
    $scope.startConstruction = function (id) {
        var buildposition = hud.getMarkerPosition();
        gameHub.server.startConstruction($scope.currentPlayer, id, buildposition.x, buildposition.y);
    };

    $("#gamescreen").click(function (e) {
        if (e.clientX || e.clientY) {
            selectGrid(e.clientX - this.offsetLeft + $(document).scrollLeft() + hud.CamPos().x, e.clientY + $(document).scrollTop() - this.offsetTop + hud.CamPos().y);
        }
    });

    function selectGrid(x, y) {
        var xTile = Math.floor(x / Game.TileSize.width);
        var yTile = Math.floor(y / Game.TileSize.height);
        setMarkerActive(true);
        
        hud.placeMarker(xTile, yTile, function () {
            gameHub.server.walkTo($scope.currentPlayer, xTile, yTile);
            setMarkerActive(false);
        });
    }

    function setMarkerActive(bool) {
        $scope.$apply(function () {
            $scope.markerSet = bool;
        });
    }

    // Incoming messages

    gameHub.client.gameStarted = function (players) {
        $scope.loadingGame = false;
        $(players).each(function (i, player) {
            var newBuilder = new CannonBuilder(5 + i, 5, i, player.Name, player.ConnectionID);
            builders.push(newBuilder);
        });
        $scope.gameState = 'ingame';
        update();
    };

    gameHub.client.joinRoom = function (room) {
        $scope.$apply(function () {
            $scope.currentRoom = room;
            $scope.gameState = 'lobby';
        });
    };

    gameHub.client.updatePlayerList = function (playerList) {
        $scope.$apply(function () {
            $scope.currentRoom.Players = playerList;
        });
    };

    gameHub.client.playerDisconnected = function (connectionID) {
        $(builders).each(function (i, item) {
            if (item.connectionID == connectionID) {
                builders.splice(i, 1);
            }
        });
    };

    gameHub.client.setRoomList = function (rooms) {
        $scope.$apply(function () {
            $scope.allRooms = rooms;
        });
    };

    gameHub.client.playerGoesTo = function (player, x, y) {
        builders[player].walkTo(x, y);
    };

    gameHub.client.playerStartsConstruction = function (player, id, x, y) {
        builders[player].build(id, { x: x, y: y });
    };

    gameHub.client.startWave = function () {
        $scope.$apply(function () {
            var enemy = new Enemy(currentLevel);
            $scope.allEnemies.push(enemy);
        });
    };

    gameHub.client.setPlayer = function (i, id) {
        $scope.$apply(function () {
            $scope.currentPlayer = i;
        });
    }

    gameHub.client.error = function (message) {
        console.log(message);
    }

    $.connection.hub.start().done(function () {

    });
}];