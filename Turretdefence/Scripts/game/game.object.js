﻿function GameObject() {
    this.speed = 4;
    this.textures = {};
    this.position = { x: 0, y: 0 };
    this.velocity = { x: 0, y: 0 };
    this.targetGrid = null;
    this.type = '';
    this.onArrive = function () { };
    this.health = 100;
    this.isAlive = true;
    this.currentAnimation = "normal";
    this.timer = 0;
    this.currentAnimationFrame = 0;
    this.animationFrameCount = false;

    this.hasArrived = function () {
        this.velocity.x = 0;
        this.velocity.y = 0;
        this.position.x = this.targetGrid.x * Game.TileSize.width;
        this.position.y = this.targetGrid.y * Game.TileSize.height;
        this.targetGrid = null;
        this.currentAnimation = "normal";
        if (this.onArrive != null)
            this.onArrive();
    };
};

GameObject.prototype.initialize = function (type, textures, position, animationFrameCount) {
    this.textures = textures;
    this.position = position;
    this.type = type;
    if (animationFrameCount != null)
        this.animationFrameCount = animationFrameCount;
    else
        this.animationFrameCount = 0;
};

GameObject.prototype.walkTo = function (xGrid, yGrid, onArrive) {
    this.targetGrid = {
        x: xGrid,
        y: yGrid
    };
    this.onArrive = onArrive;
};

GameObject.prototype.update = function () {
    if (this.targetGrid != null) {
        this.velocity.x = this.targetGrid.x - (this.position.x / Game.TileSize.width);
        this.velocity.y = this.targetGrid.y - (this.position.y / Game.TileSize.height);
        var direction = Math.sqrt(this.velocity.x * this.velocity.x + this.velocity.y * this.velocity.y);

        if (direction <= (0.01 * this.speed)) {
            this.hasArrived();
        }
        else {
            this.velocity.x /= direction;
            this.velocity.y /= direction;

            this.position.x += (this.velocity.x * this.speed);
            this.position.y += (this.velocity.y * this.speed);
            this.currentAnimation = "walkdown";
        }
    }
    if (this.timer > 2) {
        if (this.animationFrameCount > 0) {
            this.currentAnimationFrame++;
            if (this.currentAnimationFrame > this.animationFrameCount) {

                if (this.currentAnimation == "destroy") {
                    this.destroy();
                }
                else
                    this.currentAnimationFrame = 0;
            }
        }
        this.timer = 0;
    }
    this.timer++;
};

GameObject.prototype.draw = function () {
    if (this.textures != null) {
        if (this.animationFrameCount > 0) {
            if (this.textures[this.currentAnimation] != null)
                Game.PreRenderContext.drawImage(this.textures[this.currentAnimation], 50 * this.currentAnimationFrame, 0, 50, 50, this.position.x, this.position.y, Game.TileSize.width, Game.TileSize.height);
        }
        else
            Game.PreRenderContext.drawImage(this.textures["normal"], this.position.x, this.position.y, Game.TileSize.width, Game.TileSize.height);

    }
};

GameObject.prototype.increaseHealth = function (amount) {
    this.health += amount;
}

GameObject.prototype.decreaseHealth = function (amount) {
    this.health -= amount;
    if (this.health < 0) {
        this.destroy();
    }
}

GameObject.prototype.constructor = GameObject;