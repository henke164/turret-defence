﻿(function (win) {
    return win.Sound = new function () {
        var laser;

        this.loadSounds = function () {
            laser = new Audio("/Content/Sound/laser.mp3");
        };

        this.playLaserSound = function () {
            laser.play();
        }
    }
})(window);