﻿function Interface() {
    var markerTexture = new Image();
    var markerPosition = {};
    var camPos = { x: 0, y: 0 };
    this.currentAnimationFrame = 0;
    this.animationCounter = 0;

    this.CamPos = function () {
        return camPos;
    };

    this.initialize = function (type) {
        markerTexture.src = "Content/Images/mark.png";
        Game.PreRenderContext.translate(0, 0);
    };

    this.placeMarker = function (x, y, moveToCallback) {
        if (markerPosition.x == x && markerPosition.y == y) {
            moveToCallback();
            markerPosition = {};
        }
        else {
            markerPosition = { x: x, y: y };
        }
    };

    this.getMarkerPosition = function () {
        return markerPosition;
    };

    this.draw = function () {
        if (markerPosition.x != null && markerPosition.y != null) {

            Game.PreRenderContext.drawImage(markerTexture, 50 * this.currentAnimationFrame, 0, 50, 50,
                    (markerPosition.x * Game.TileSize.width),
                    (markerPosition.y * Game.TileSize.height),
                    Game.TileSize.width, Game.TileSize.height);

            if (this.animationCounter > 7) {
                this.currentAnimationFrame++;
                this.animationCounter = 0;
            }
            this.animationCounter++;

            if (this.currentAnimationFrame > 2)
                this.currentAnimationFrame = 0;
        }

    };

    this.checkKey = function(e){
        e = e || window.event;
        if (e.keyCode == '37') {
            camPos.x -= 10;
            Game.PreRenderContext.translate(10, 0);
        }
        else if (e.keyCode == '38') {
            camPos.y -= 10;
            Game.PreRenderContext.translate(0, 10);
        }
        else if (e.keyCode == '39') {
            camPos.x += 10;
            Game.PreRenderContext.translate(-10, 0);
        }
        else if (e.keyCode == '40') {
            camPos.y += 10;
            Game.PreRenderContext.translate(0, -10);
        }
    }
}