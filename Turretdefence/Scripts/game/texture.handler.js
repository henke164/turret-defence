﻿(function (win) {
    return win.Textures = new function () {

        this.Images = [];
        var missileNormal = new Image();
        var turret1Normal = new Image();
        

        // Builder
        var builder_Blue_Normal = new Image();
        var builder_Blue_WalkDown = new Image();
        var builder_Red_Normal = new Image();
        var builder_Red_WalkDown = new Image();

        // Enemies
        var enemy1 = new Image();
        var enemy1WalkDown = new Image();
        var enemy1Destroy = new Image();

        // HUD
        var progressBar = new Image();

        this.loadTextures = function () {
            missileNormal.src = "Content/Images/missile.png";

            // Cannons
            turret1Normal.src = "Content/Images/turret_1.png";

            // Builder
            builder_Blue_Normal.src = "Content/Images/cat_blue.png";
            builder_Blue_WalkDown.src = "Content/Images/cat_blue_walk_down.png";

            builder_Red_Normal.src = "Content/Images/cat_red.png";
            builder_Red_WalkDown.src = "Content/Images/cat_red_walk_down.png";

            // Enemies
            enemy1.src = "Content/Images/enemy_1.png";
            enemy1WalkDown.src = "Content/Images/enemy_1_walk_down.png";
            enemy1Destroy.src = "Content/Images/enemy_1_destroy.png";

            // HUD
            progressBar.src = "Content/Images/progressBar.jpg";
        };

        this.missile = function () {
            var texture = {};
            texture["normal"] = missileNormal;
            return texture;
        };

        this.cannon = function () {
            var texture = {};
            texture["normal"] = turret1Normal;
            return texture;
        };

        this.builder = function (player) {
            switch (player)
            {
                case 0:
                    var texture = {};
                    texture["normal"] = builder_Blue_Normal;
                    texture["walkdown"] = builder_Blue_WalkDown;
                    return texture;
                    break;
                case 1:
                    var texture = {};
                    texture["normal"] = builder_Red_Normal;
                    texture["walkdown"] = builder_Red_WalkDown;
                    return texture;
                    break;
            }
        };

        this.enemy = function () {
            var texture = {};
            texture["normal"] = enemy1;
            texture["walkdown"] = enemy1WalkDown;
            texture["destroy"] = enemy1Destroy;
            return texture;
        };

        this.progressBar = function () {
            return progressBar;
        }
    }
})(window);