﻿var Cannon = function (id, position, onFinishedCallback) {
    GameObject.call(this);
    this.health = 0;
    this.maxHealth = 200;
    this.onFinished = onFinishedCallback;

    var isAlive = false;
    var shootTarget = null;
    var isShooting = false;
    var shootingInterval = {};
    var traceLightCounter = 0;
    var missiles = [];

    var missileObj = new Missile({ x: position.x, y: position.y });

    // turret Specifics
    var weaponType = "missiles";
    var damage = 10;
    var range = 5;
    var reloadTime = 5;
    var traceLight = {
        color: 'yellow',
        time: 5,
        size: 2
    };
    var buildSpeed = 2;
    var laserSound = new Audio("/Content/Sound/laser.mp3");

    this.update = function () {
        // Building
        if (!isAlive) {
            if (this.health < this.maxHealth)
                this.increaseHealth(buildSpeed);
            else {
                isAlive = true;
                this.onFinished();
            }
        }
        else {
            // Find close targets
            var gameScope = angular.element($('#gamewindow')).scope();
            var turretPosition = this.position;
            var hasTargetInDistance = false;
            $(gameScope.allEnemies).each(function (i, enemy) {
                var distance = Math.sqrt(
                    Math.pow((enemy.position.x / Game.TileSize.width) - (turretPosition.x / Game.TileSize.width), 2) +
                    Math.pow((enemy.position.y / Game.TileSize.height) - (turretPosition.y / Game.TileSize.height), 2));

                if (distance < range) {
                    hasTargetInDistance = true;
                    if (isShooting == false)
                        shootAtTarget(turretPosition, enemy);
                }
            });


            if (!hasTargetInDistance) {
                stopFire();
            }
        }

        // Check if target is dead
        if (isShooting == true) {
            var gameScope = angular.element($('#gamewindow')).scope();
            var targetArrayIndex = gameScope.allEnemies.indexOf(shootTarget);
            if (targetArrayIndex == -1) {
                isShooting = false;
                stopFire();
            }
        }

        // Update missiles
        $(missiles).each(function (i, missile) {
            missile.update();
        });

        Cannon.prototype.update.call(this);
    };

    this.draw = function () {
        $(missiles).each(function (i, missile) {
            missile.draw();
        });

        Cannon.prototype.draw.call(this);
    };

    this.drawHud = function () {
        if (isShooting == true && traceLightCounter > 0) {
            Game.PreRenderContext.lineWidth = traceLight.size;
            Game.PreRenderContext.moveTo(this.position.x + (Game.TileSize.width / 2), this.position.y + (Game.TileSize.height / 2));
            Game.PreRenderContext.lineTo(shootTarget.position.x + (Game.TileSize.width / 2), shootTarget.position.y + (Game.TileSize.height / 2));
            Game.PreRenderContext.strokeStyle = traceLight.color;
            traceLightCounter--;
        }
    };

    function shootAtTarget(turretPosition, target) {
        shootTarget = target;
        isShooting = true;
        shootingInterval = setInterval(function () {

            if (weaponType == "missiles") {
                laserSound.play();
                var missile = jQuery.extend(true, {}, missileObj);
                missile.target = target;
                missile.onHitCallback = function () {
                    target.decreaseHealth(damage);
                    missiles.splice(missiles.indexOf(missile), 1);
                }
                missiles.push(missile);
            }
            else if (weaponType == "laser") {
                target.decreaseHealth(damage);
                traceLightCounter = traceLight.time;
            }

        }, reloadTime * 100);
    };

    function stopFire() {
        isShooting = false;
        clearInterval(shootingInterval);
    };

    this.initialize("cannon", Textures.cannon(), position);
};
Cannon.prototype = GameObject.prototype;