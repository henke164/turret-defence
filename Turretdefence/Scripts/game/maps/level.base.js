﻿function Level() {
    var worldSize = { width: 30, height: 20 };
    var backgroundTile = new Image();

    this.enemyPath = [
        { x: -1, y: 6 },
        { x: 5, y: 6 },
        { x: 5, y: 7 },
        { x: 10, y: 7 },
        { x: 10, y: 6 },
        { x: 15, y: 6 },
    ];

    this.initialize = function (type) {
        if (type == 'grass')
            backgroundTile.src = "Content/Images/grass.jpg";
        if (type == 'sand')
            backgroundTile.src = "Content/Images/sand.jpg";
    }

    this.draw = function () {
        for (var x = 0; x < worldSize.width; x++) {
            for (var y = 0; y < worldSize.height; y++) {
                Game.PreRenderContext.drawImage(backgroundTile, (x * Game.TileSize.width), (y * Game.TileSize.height), Game.TileSize.width, Game.TileSize.height);
            }
        }
    }
}