﻿var CannonBuilder = function (startPosX, startPosY, player, name, connectionID) {
    GameObject.call(this);
    this.connectionID = connectionID;
    this.name = name;
    var isBusy = false;
    var buildingCannon = null;
    var startPosition = {
        x: (startPosX * Game.TileSize.width),
        y: (startPosY * Game.TileSize.height)
    };
    var speechBubble = null;

    this.build = function (cannonId, position) {
        this.say("yes sir");
        this.walkTo(position.x, position.y, function () {
            isBusy = true;
            var gameScope = angular.element($('#gamewindow')).scope();

            var newCannon = new Cannon(cannonId, {
                x: position.x * Game.TileSize.width,
                y: position.y * Game.TileSize.height
            }, function () {
                isBusy = false;
            });
            buildingCannon = newCannon;
            gameScope.myCannons.push(newCannon);
        });
    };

    this.say = function (text) {
        speechBubble = {
            date: new Date(),
            text: text
        }
        setTimeout(function () {
            speechBubble = null;
        }, 2000);
    };
    
    this.walkTo = function (x, y, onArrive) {
        if (!isBusy) {
            this.say("Mööving forward!");
            CannonBuilder.prototype.walkTo.call(this, x, y, onArrive);
        }
        else {
            this.say("Im busy, one sec!");
        }
    };

    this.update = function () {
        CannonBuilder.prototype.update.call(this);
    };

    this.draw = function () {        
        CannonBuilder.prototype.draw.call(this);
    };

    this.drawHud = function () {
        if (speechBubble != null) {
            Game.PreRenderContext.fillStyle = 'white';
            Game.PreRenderContext.fillText(speechBubble.text, this.position.x, this.position.y);
        }

        Game.PreRenderContext.fillStyle = 'white';
        Game.PreRenderContext.fillText(this.name, this.position.x, this.position.y);

        if (isBusy && buildingCannon != null) {
            Game.PreRenderContext.drawImage(Textures.progressBar(), 0, 0, (buildingCannon.health / buildingCannon.maxHealth) * Textures.progressBar().width, 10, this.position.x, this.position.y - 10, (buildingCannon.health / buildingCannon.maxHealth) * Game.TileSize.width, 10);
        }
    };

    this.initialize("builder", Textures.builder(player), startPosition, 5, true);
};
CannonBuilder.prototype = GameObject.prototype;