﻿var Enemy = function (map) {
    GameObject.call(this);
    var startPosition = {
        x: map.enemyPath[0].x * Game.TileSize.width,
        y: map.enemyPath[0].y * Game.TileSize.height
    };

    var targetPathIndex = 0;
    this.speed = 5;

    this.keepMoving = function () {
        if (map.enemyPath.length > targetPathIndex + 1) {
            targetPathIndex++;
            this.walkTo(map.enemyPath[targetPathIndex].x, map.enemyPath[targetPathIndex].y, this.keepMoving);
        }
        else {
            this.destroy();
        }
    };

    this.walkTo = function (x, y, onArrive) {
        Enemy.prototype.walkTo.call(this, x, y, onArrive);
    };

    this.update = function () {
        Enemy.prototype.update.call(this);
    };

    this.draw = function () {
        Enemy.prototype.draw.call(this);
    };

    this.drawHud = function () {
        Game.PreRenderContext.fillStyle = 'white';
        Game.PreRenderContext.fillText(this.health, this.position.x, this.position.y);
    };

    this.destroy = function () {
        var gameScope = angular.element($('#gamewindow')).scope();
        if (this.isAlive == true) {
            this.isAlive = false;
            gameScope.allEnemies.splice(gameScope.allEnemies.indexOf(this), 1);
        }
    };

    this.initialize("enemy", Textures.enemy(), startPosition, 5, true);
    this.keepMoving();
};
Enemy.prototype = GameObject.prototype;